" =========  general config ==========
set nu
set wrap
syntax on
set nocompatible
" source /usr/share/vim/google/google.vim

" ========= Indentation ==============
set tabstop=2
set shiftwidth=2
set expandtab
set autoindent
set smartindent
set smarttab

filetype plugin on
filetype indent on

" ========= Vundle ===================
if filereadable(expand("~/.vim/vundles.vim"))
  source ~/.vim/vundles.vim
endif

" ========= Settings =================
if filereadable(expand("~/.vim/settings.vim"))
  source ~/.vim/settings.vim
endif

" ========= Color ====================
syntax enable
