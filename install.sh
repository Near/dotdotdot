#!/bin/sh

if [ ! -d "$HOME/.dotdotdot"  ]; then
  echo "Installing DOTDOTDOT for the first time"
  git clone https://Near@bitbucket.org/Near/dotdotdot.git "$HOME/.dotdotdot"
  touch "$HOME/.zshrc_config"
  ln -s "$HOME/.dotdotdot/vimrc" "$HOME/.vimrc"
  ln -s "$HOME/.dotdotdot/zshrc" "$HOME/.zshrc"
  ln -s "$HOME/.dotdotdot/vim" "$HOME/.vim"
else
  echo "DOTDOTDOT is already installed"
fi
