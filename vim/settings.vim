" main settings
set wrap
let mapleader=";"

" import settings for plugins
for fpath in split(globpath('~/.vim/settings', '*.vim'), '\n')
  exe 'source' fpath
endfor
