let g:syntastic_cpp_check_header = 1
let g:syntastic_cpp_compiler_options = '-std=c++1y'
let g:syntastic_auto_jump = 0
let g:syntastic_auto_loc_list = 1
let g:syntastic_quiet_messages = {'level': 'warnings'}
