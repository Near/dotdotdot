set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'
" Dash - using dash documents
Plugin 'rizzatti/dash.vim'
" Supertab - completions
Plugin 'ervandew/supertab'
" Syntastic - syntax checking
Plugin 'scrooloose/syntastic'
" Auto pairs - insert brackets etc. automatically
Plugin 'jiangmiao/auto-pairs'
" Nerd commenter - help you comment stuff
Plugin 'scrooloose/nerdcommenter'
" Nerdtree - file explorer
Plugin 'scrooloose/nerdtree'
" Taglist - source code browser
Plugin 'vim-scripts/taglist.vim'
" Vim lightline
Plugin 'itchyny/lightline.vim'
" Vim color solarized
Plugin 'altercation/vim-colors-solarized'
" Plugin for scala
Plugin 'jergason/scala.vim'
" Plugin for rust vim
Plugin 'wting/rust.vim'
" Plugin for switch from .c to .h
Plugin 'vim-scripts/a.vim'
" Plugin for indexer : automatically call ctags
Plugin 'vim-scripts/DfrankUtil'
Plugin 'vim-scripts/vimprj'
Plugin 'vim-scripts/indexer.tar.gz'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList          - list configured plugins
" :PluginInstall(!)    - install (update) plugins
" :PluginSearch(!) foo - search (or refresh cache first) for foo
" :PluginClean(!)      - confirm (or auto-approve) removal of unused plugins
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line
