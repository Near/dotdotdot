# variables
# source your own zshrc files
source "$HOME/.zshrc_config"

# auto complete
autoload -U compinit
compinit

# color
autoload -U colors
colors

# prompt
autoload -U promptinit
promptinit
PROMPT="%{$fg[blue]%}%n%{$reset_color%}@%{$fg[red]%}%m %{$fg[green]%}%2~ %{$reset_color%}%# "
source "$HOME/.dotdotdot/zsh/prompt.zsh"

#####################################
# key bindings
bindkey -v
bindkey -s "jj" "\e"
